Try to predict if a patient has diabetes

The program use scikit-learn Python module (http://scikit-learn.org/)

Data file are extracted from UCI Machine Learning repository (Lichman, M. (2013). UCI Machine Learning Repository [http://archive.ics.uci.edu/ml]. Irvine, CA: University of California, School of Information and Computer Science.)

We first do a nested cross validation to search for best classifer, then we refine the parameters of best classfier found with cross validation.
Best method give accuracy of 75-77%, which is coherent with studies on the data set (http://www.fizyka.umk.pl/kis/projects/datasets.html#Diabetes). We can slightly increase the accuracy by using a bagging ensamble method.

See https://archive.ics.uci.edu/ml/datasets/Pima+Indians+Diabetes for a description of data.

See COPYING for distribution licence.

== Python packages required ==

* scikit-learn: Machine learning for Python
* NumPy: 
* pandas: data manipulation
* Matplotlib: Data plotting
* tabulate: pretty prints tables

== Usage ==
For Linux (and probably Mac) you can launch ./run.sh via terminal
For all platforms, launch "python3 src/diabetes_predictor.py" from tha home dir of the project.

Copyright 2016 Luca Antonelli
luke.anto@gmail.com


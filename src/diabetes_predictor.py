#################################################################################
#                                                                               #
# Copyright 2016 Luca Antonelli                                                 #
# luke.anto@gmail.com                                                           #
#                                                                               #
# This file is part of Diabetes Predictor.                                      #
#                                                                               #
# Diabetes Predictor is free software: you can redistribute it and/or modify    #
# it under the terms of the GNU General Public License as published by          #
# the Free Software Foundation, either version 3 of the License, or             #
# (at your option) any later version.                                           #
#                                                                               #
# Diabetes Predictor is distributed in the hope that it will be useful,         #
# but WITHOUT ANY WARRANTY; without even the implied warranty of                #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                 #
# GNU General Public License for more details.                                  #
#                                                                               #
# You should have received a copy of the GNU General Public License             #
# along with Diabetes Predictor.  If not, see <http://www.gnu.org/licenses/>.   #
#                                                                               #
#################################################################################

# Script used to check Diabetes Predictor

import os
import numpy as np

from data_manager import DataLoader

from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler, Imputer

from sklearn.cross_validation import train_test_split

from sklearn.linear_model import SGDClassifier, LogisticRegression
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, BaggingClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.ensemble import AdaBoostClassifier

from classifier import DiabetesClassifier
from data_manager import DiabetesDataLoader, DataLoader

# Step 0: Base variables
# Random seed (set None to have every time different results)
RANDOM_STATE = 0
# Output dir
OUT_DIR = "output"
if not os.path.exists(OUT_DIR):
  os.makedirs(OUT_DIR)
else:
  for f in [ f for f in os.listdir(OUT_DIR)]:
    os.remove(os.path.join(OUT_DIR, f))

# Step 1: Load Data
columnNames = "Pregnancies", "Glucose", "Pressure", "Triceps", "Insulin", "BMI", "Pedigree", "Age", "Diabetes"
data = DataLoader(columnNames).load(os.path.join("data", "pima-indians-diabetes.data"))

# Step 2: As noted in UCI site, some columns has suspect zero values, probably meaning a NaN values; let's check for them
from collections import Counter
zeroes = { column : Counter(data[column])[0] for column in columnNames }
print("Zeroes before post-data managing...")
print(zeroes)
# So we use a more specifice data loader which include a post-data treatment to substitute 0 ==> NaN for suitable columns
data = DiabetesDataLoader().load(os.path.join("data", "pima-indians-diabetes.data"))
zeroes = { column : Counter(data[column])[0] for column in columnNames }
print("Zeroes after NaN valorization...")
print(zeroes)

# Step 3: Managing N/A
# there are too many rows with N/A, so we cannot simply remove them
print("{0:d} lines loaded. {1:d} of them have no N/A values".format(len(data.index), len(data.dropna().index)))
# we better substitute missing values with the mean of each column
# This is a step to introduce in predictor pipeline
imputerStep = ('nan', Imputer())
nanStrategies = ['mean', 'median', 'most_frequent']

# Step 4: extract data for training and testing
X = data.iloc[:,:-1]
y = data.iloc[:,-1]
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.25, random_state = RANDOM_STATE, stratify = y)

# Step 5: First Comparison of Classifier with Nested Cross Validation
# Output shows Random Forest, Linear SVM with SGD and Logistic regrssion with SGD ad best estimators.
# Linear SVM seems the most stable

# Collect algorithms to test
algorithms = []

# Support Vector Machine
svm = Pipeline(steps = [imputerStep,
                        ('scl', StandardScaler()),
                        ('clf', SVC(random_state = RANDOM_STATE, degree = 5)),])
algorithms.append({'name': 'svm',
                   'estimator': svm,
                   'paramGrid': {'nan__strategy': nanStrategies,
                                 'clf__C': np.logspace(-4, 1, num = 6, base = 10),
                                 'clf__kernel': ['poly', 'poly']}
                  })


# K Nearest Neighbors
knn = Pipeline(steps = [imputerStep,
                        ('scl', StandardScaler()),
                        ('clf', KNeighborsClassifier()),])
algorithms.append({'name': 'knn',
                   'estimator': knn,
                   'paramGrid': {'nan__strategy': nanStrategies,
                                 'clf__p': [1, 2],
                                 'clf__weights': ['uniform', 'distance']}
                  })

# Decision Tree
decisionTree = Pipeline(steps = [imputerStep,
                                 ('tree', DecisionTreeClassifier(random_state = RANDOM_STATE))])
algorithms.append({'name': 'decisionTree',
                   'estimator': decisionTree,
                   'paramGrid': {'nan__strategy': nanStrategies,
                                 'tree__max_depth': [3, 4, 5, 6, 7, 8, 9],
                                 'tree__class_weight': [None, 'balanced']}
                  })

# Random Forest
randomForest = Pipeline(steps = [imputerStep,
                                 ('rndf', RandomForestClassifier(random_state = RANDOM_STATE))])
algorithms.append({'name': 'randomForest',
                   'estimator': randomForest,
                   'paramGrid': {'nan__strategy': nanStrategies,
                                 'rndf__n_estimators': [10, 20],
                                 'rndf__criterion': ['gini', 'entropy'],
                                 'rndf__max_depth': [3, 4, 5, 6, 7, 8, 9],
                                 'rndf__class_weight': [None, 'balanced', 'balanced_subsample']}
                  })
# Naive Bayes
bayes = Pipeline(steps = [imputerStep,
                          ('bay', MultinomialNB())])
algorithms.append({'name': 'bayes',
                   'estimator': bayes,
                   'paramGrid': {'nan__strategy': nanStrategies,
                                 'bay__fit_prior': [False, True]}
                  })

# SGD - Linear Support Vector Machine
SGDLinearSVM = Pipeline(steps = [imputerStep,
                                 ('scl', StandardScaler()),
                                 ('clf', SGDClassifier(loss='hinge', random_state = RANDOM_STATE)),])
algorithms.append({'name': 'SGDLinearSVM',
                   'estimator': SGDLinearSVM,
                   'paramGrid': {'nan__strategy': nanStrategies,
                                 'clf__alpha': np.logspace(-7, 3, num = 11, base = 10) }
                  })

# SGD - Linear Support Vector Machine with Quadratic Penalization
SGDLinearSVMSquared = Pipeline(steps = [imputerStep,
                                        ('scl', StandardScaler()),
                                        ('clf', SGDClassifier(loss='squared_hinge', random_state = RANDOM_STATE)),])
algorithms.append({'name': 'SGDLinearSVMSquared',
                   'estimator': SGDLinearSVMSquared,
                   'paramGrid': {'nan__strategy': nanStrategies,
                                 'clf__alpha': np.logspace(-7, 3, num = 11, base = 10) }
                  })

# SGD - Logistic Regression
SGDLogisticRegression = Pipeline(steps = [imputerStep,
                                          ('scl', StandardScaler()),
                                          ('clf', SGDClassifier(loss='log', random_state = RANDOM_STATE)),])
algorithms.append({'name': 'SGDLogisticRegression',
                   'estimator': SGDLogisticRegression,
                   'paramGrid': {'nan__strategy': nanStrategies,
                                 'clf__alpha': np.logspace(-7, 3, num = 11, base = 10) }
                  })

adaBoost = Pipeline(steps = [imputerStep,
                             ('scl', StandardScaler()),
                             ('clf', AdaBoostClassifier(random_state = RANDOM_STATE, algorithm = "SAMME")),])
algorithms.append({'name': 'adaBoost',
                   'estimator': adaBoost,
                   'paramGrid': {'nan__strategy': nanStrategies,
                                 'clf__base_estimator': [SGDClassifier(loss='squared_hinge', random_state = RANDOM_STATE)],
                                 'clf__n_estimators': [50, 100, 200],
                                 'clf__learning_rate': [0.1, 1.0]}
                  })

classifier = DiabetesClassifier()
classifier.nestedCrossValidation(algorithms, X_train, y_train, outDir = OUT_DIR)

# Step 6: Parameter tuning for best algorithms
algorithms = []
algorithms.append({'name': 'randomForest',
                   'estimator': randomForest,
                   'paramGrid': {'nan__strategy': nanStrategies,
                                 'rndf__n_estimators': np.linspace(10, 20, num = 3, dtype = 'int'),
                                 'rndf__criterion': ['gini', 'entropy'],
                                 'rndf__max_depth': np.linspace(3, 11, num = 9, dtype = 'int'),
                                 'rndf__class_weight': [None, 'balanced', 'balanced_subsample']}
                  })
algorithms.append({'name': 'SGDLinearSVM',
                   'estimator': SGDLinearSVM,
                   'paramGrid': {'nan__strategy': nanStrategies,
                                 'clf__alpha': np.logspace(-10, 5, num = 30, base = 10) }
                  })
algorithms.append({'name': 'SGDLogisticRegression',
                   'estimator': SGDLogisticRegression,
                   'paramGrid': {'nan__strategy': nanStrategies,
                                 'clf__alpha': np.logspace(-10, 5, num = 30, base = 10) }
                  })

bestEstimators = []
for algorithm in algorithms:
  algorithm['bestEstimator'] = classifier.crossValidation(algorithm, X_train, y_train, outDir = OUT_DIR)

# Checking best algorithms metrics
for algorithm in algorithms:
  classifier.metrics(algorithm['bestEstimator'], algorithm['name'], X_train, y_train, X_test, y_test, outDir = OUT_DIR)
  
# Trying Bagging on algorithms
baggingAlgorithms = []
for algorithm in algorithms:
  baggingAlgorithm = {'name' : 'bagging_' + algorithm['name'],
                       'algorithm' : BaggingClassifier(base_estimator = algorithm['bestEstimator'], random_state = RANDOM_STATE)}
  classifier.metrics(Pipeline(steps = [imputerStep,
                                      ('bag', baggingAlgorithm['algorithm'])]), 
                     baggingAlgorithm['name'], 
                     X_train, y_train, X_test, y_test, outDir = OUT_DIR)
  baggingAlgorithms.append(baggingAlgorithm)


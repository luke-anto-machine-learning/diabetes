#################################################################################
#                                                                               #
# Copyright 2016 Luca Antonelli                                                 #
# luke.anto@gmail.com                                                           #
#                                                                               #
# This file is part of Diabetes Predictor.                                      #
#                                                                               #
# Diabetes Predictor is free software: you can redistribute it and/or modify    #
# it under the terms of the GNU General Public License as published by          #
# the Free Software Foundation, either version 3 of the License, or             #
# (at your option) any later version.                                           #
#                                                                               #
# Diabetes Predictor is distributed in the hope that it will be useful,         #
# but WITHOUT ANY WARRANTY; without even the implied warranty of                #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                 #
# GNU General Public License for more details.                                  #
#                                                                               #
# You should have received a copy of the GNU General Public License             #
# along with Diabetes Predictor.  If not, see <http://www.gnu.org/licenses/>.   #
#                                                                               #
#################################################################################

import numpy as np
import matplotlib.pyplot as plt
import os

from sklearn.grid_search import GridSearchCV
from sklearn.cross_validation import cross_val_score
from sklearn.metrics import confusion_matrix, classification_report
from matplotlib import rcParams
from tabulate import tabulate

class Classifier:
  """Generic tools for classifier selection."""
  
  _targetClassNames = ["0", "1"]
  
  def __init__(self):
    # Configuration definition for the class
    # Scoring method used to cross validate algorithms
    self._scoring = 'accuracy'
    # Number of parallel jobs while cross validating algorithms
    self._n_jobs = 2
    # Note we use the 5x2 cross-validation folding
    self._cvFoldingGridSearch = 5
    self._cvFoldingCrossValidation = 5
    rcParams.update({'figure.autolayout': True})
    
  def nestedCrossValidation(self, algorithms, X_train, y_train, outDir = None):
    """Execute a nested cross validation on a list of algorithms.
       Each element of the list is a dictionary with these elements;
       * id
       * estimator 
       * paramGrid 
    """
    if outDir:
      txtFilePath = os.path.join(outDir, "nestedCrossValidation.txt")
      plotFilePath = os.path.join(outDir, "nestedCrossValidation.png")
    else:
      txtFilePath = None
      plotFilePath = None
    scores = []
    labels = []
    for algorithm in algorithms:
      print("Evaluating {0:s} method...".format(algorithm['name']))
      gs = GridSearchCV(estimator = algorithm['estimator'],
                        param_grid = algorithm['paramGrid'],
                        scoring = self._scoring,
                        cv = self._cvFoldingGridSearch)
      crossValidationScores = cross_val_score(estimator = gs,
                                              X = X_train,
                                              y = y_train,
                                              scoring = self._scoring,
                                              cv = self._cvFoldingCrossValidation)
      scores.append(crossValidationScores)
      labels.append(algorithm['name'])
    
    # Plot data
    if len(scores) > 0:
      self._printNestedCrossValidation(scores = scores, labels = labels, filePath = txtFilePath)
      self._plotNestedCrossValidation(scores = scores, labels = labels, filePath = plotFilePath)
    else:
      raise UserWarning('No data to show.')
    
  def crossValidation(self, algorithm, X_train, y_train, outDir = None):
    """Make cross validation on a single algorithm, to tune its parameters"""
    print("Tuning {0:s} method...".format(algorithm['name']))
    if outDir:
      filePath = os.path.join(outDir, "crossValidation.txt")
    else:
      filePath = None
    gs = GridSearchCV(estimator = algorithm['estimator'],
                              param_grid = algorithm['paramGrid'],
                              cv = 10,
                              scoring = self._scoring)
    gs = gs.fit(X_train, y_train)
    self._printCrossValidation(estimatorName = algorithm['name'], gridSearchData = gs, filePath = filePath)
    return gs.best_estimator_
  
  def metrics(self, estimator, estimatorName, X_train, y_train, X_test, y_test, outDir = None):
    """Get metrics for the classifier."""
    print("Creating {0:s} metrics...\n".format(estimatorName))
    y_pred = estimator.fit(X_train, y_train).predict(X_test)

    # Classification Report
    if outDir:
      classificationReportFilePath = os.path.join(outDir, "classificationReport.txt")
    else:
      classificationReportFilePath = None
    classificationReport = estimatorName + "\n\n" + classification_report(y_test, y_pred, target_names = Classifier._targetClassNames)
    print(classificationReport)
    if classificationReportFilePath:
      with open(classificationReportFilePath, 'a') as fd:
        print(classificationReport, file = fd)
        
    # Confusion Matrix
    if outDir:
      confusionMatrixFilePath = os.path.join(outDir, estimatorName + "_confusionMatrix.png")
    else:
      confusionMatrixFilePath = None
    self._plotConfusionMatrix(estimator, estimatorName, y_test, y_pred, filePath = confusionMatrixFilePath)
  
  def _plotNestedCrossValidation(self, scores, labels, filePath = None):
    plt.boxplot(x = scores, labels = labels)
    plt.ylabel('Score')
    plt.xlabel('Method')
    plt.xticks(rotation = 45)
    plt.title('Nested Cross Validation')
    
    print("See boxplot in the other window...")
    print()
    fig = plt.gcf()
    plt.show()
    if filePath:
      fig.savefig(filePath)

  def _printNestedCrossValidation(self, scores, labels, filePath = None):
    algorithm_scores = ["{0:1.3f} +/- {1:1.3f}".format(np.mean(score), np.std(score)) for score in scores]
    data = zip(labels, algorithm_scores)
    
    outputText = "Cross Validation results" + "\n"
    outputText += "\n"
    outputText += tabulate(data, headers=["Algorithm", "Score"])
    outputText += "\n"
    
    print(outputText)
    if filePath:
      with open(filePath, 'a') as fd:
        print(outputText, file = fd)

  def _printCrossValidation(self, estimatorName, gridSearchData, filePath = None):
    outputText = "Best score = {0:2.2f}% for {1:s} reached with parameters:\n".format(gridSearchData.best_score_ * 100, estimatorName)
    outputText += "\n"
    outputText += tabulate(gridSearchData.best_params_.items(), headers=["Parameter", "Value"])
    outputText += "\n"
    
    print(outputText)
    if filePath:
      with open(filePath, 'a') as fd:
        print(outputText, file = fd)

  def _plotConfusionMatrix(self, estimator, estimatorName, y_test, y_pred, filePath):
    
    confusionMatrix = confusion_matrix(y_test, y_pred)
    
    # Image
    plt.imshow(confusionMatrix, interpolation = 'nearest', cmap = plt.cm.Blues)

    # Title and color bar
    plt.title('Confusion Matrix - {0:s}'.format(estimatorName))
    plt.colorbar()
    
    # Axis
    marks = Classifier._targetClassNames
    tick_marks = np.arange(len(marks))
    plt.xticks(tick_marks, marks, rotation=45)
    plt.yticks(tick_marks, marks)

    plt.ylabel('True class')
    plt.xlabel('Predicted class')
    
    fig = plt.gcf()
    plt.show()
    if filePath:
      fig.savefig(filePath)

class DiabetesClassifier(Classifier):
  """Specific Classifier for Diabetes Predictor."""
  
  _targetClassNames = ["No", "Yes"]
  
  def __init__(self):
    super().__init__()
    self._cvFoldingCrossValidation = 2


if __name__ == '__main__':

  import os
  
  from data_manager import DataLoader
  from sklearn.pipeline import Pipeline
  from sklearn.preprocessing import StandardScaler
  from sklearn.linear_model import SGDClassifier, LogisticRegression
  
  classifier = Classifier()
  linearSVM = Pipeline(steps = [('scl', StandardScaler()),
                                ('clf', SGDClassifier(loss='hinge', random_state = 0)),])
  logisticRegression = Pipeline(steps = [('scl', StandardScaler()),
                                         ('clf', LogisticRegression(random_state = 0)),])

  algorithms = [
                {'name': 'linearSVM',
                 'estimator': linearSVM,
                 'paramGrid': {'clf__alpha': np.logspace(-6, 0, num = 7, base = 10) }
                },
                {'name': 'logistic',
                 'estimator': logisticRegression,
                 'paramGrid': {'clf__C': np.logspace(-3, 3, num = 7, base = 10) }
                },
               ]
  columnNames = "Pregnancies", "Glucose", "Pressure", "Triceps", "Insulin", "BMI", "Pedigree", "Age", "Diabetes"
  data = DataLoader(columnNames).load(os.path.join("data", "pima-indians-diabetes.data"))
  X_train = data.iloc[:,:-1]
  y_train = data.iloc[:,-1]
  classifier.nestedCrossValidation(algorithms, X_train, y_train)
  
